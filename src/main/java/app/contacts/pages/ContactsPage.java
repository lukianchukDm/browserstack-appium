package app.contacts.pages;

import io.appium.java_client.MobileBy;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import org.openqa.selenium.By;

import java.util.List;

public class ContactsPage extends BasePageObject {

    private By searchInputLocator = MobileBy.id("com.jayway.contacts:id/main_search");
    private By contactLocator = MobileBy.xpath("//*[@resource-id='com.jayway.contacts:id/name']");

    public ContactsPage(AndroidDriver<AndroidElement> driver) {
        super(driver);
    }

    public ContactsPage searchContact(String contact) {
        fill(contact, searchInputLocator);

        return this;
    }

    public String getContact(int index) {
        List<AndroidElement> contacts = findAll(contactLocator);

        return contacts.get(index).getText();
    }

    public ContactDetailsPage openResult(int index) {
        List<AndroidElement> contacts = findAll(contactLocator);
        contacts.get(index).click();

        return new ContactDetailsPage(driver);
    }
}
