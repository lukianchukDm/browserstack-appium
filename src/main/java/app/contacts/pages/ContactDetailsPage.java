package app.contacts.pages;

import io.appium.java_client.MobileBy;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import org.openqa.selenium.By;

public class ContactDetailsPage extends BasePageObject {

    private By nameLocator = MobileBy.id("com.jayway.contacts:id/detail_name");
    private By phoneLocator = MobileBy.id("com.jayway.contacts:id/phonenumber");

    public ContactDetailsPage(AndroidDriver<AndroidElement> driver) {
        super(driver);
    }

    public String getFullname() {
        return getText(nameLocator);
    }

    public String getPhoneNumber() {
        return getText(phoneLocator);
    }
}
