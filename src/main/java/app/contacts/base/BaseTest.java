package app.contacts.base;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.ITestContext;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;

import java.io.IOException;
import java.lang.reflect.Method;
import java.net.URL;
import java.util.HashMap;

@Listeners({TestListener.class})
public class BaseTest {

    protected String testSuiteName;
    protected String testName;
    protected String testMethodName;

    protected AndroidDriver<AndroidElement> driver;

    public void initializeDriver() throws IOException {
        DesiredCapabilities caps = new DesiredCapabilities();

        caps.setCapability("device", "Google Pixel 3");
        caps.setCapability("os_version", "9.0");
        caps.setCapability("project", "Browserstack + Appium");
        caps.setCapability("build", "0.1");
        caps.setCapability("name", "Contacts App Test");
        caps.setCapability("app", "bs://dc41139999cf03e7aec4b589dac3d911b3ecf303");

        TestUtilities utilities = new TestUtilities();
        HashMap<String, String> credentials = utilities.readCredentials();

        String username = credentials.get("username");
        String accessKey = credentials.get("accessKey");

        String bsUrl = "https://" + username + ":" + accessKey + "@hub-cloud.browserstack.com/wd/hub";

        driver = new AndroidDriver<AndroidElement>(new URL(bsUrl), caps);
    }

    @BeforeMethod(alwaysRun = true)
    public void setUp(Method method, ITestContext context) throws IOException {
        initializeDriver();

        this.testSuiteName = context.getSuite().getName();
        this.testName = context.getCurrentXmlTest().getName();
        this.testMethodName = method.getName();
    }

    @AfterMethod(alwaysRun = true)
    public void tearDown() {
        driver.quit();
    }

}


