package app.contacts;

import app.contacts.base.TestUtilities;
import app.contacts.pages.ContactDetailsPage;
import app.contacts.pages.ContactsPage;
import org.testng.Assert;
import org.testng.annotations.Test;

public class SimpleTests extends TestUtilities {

    @Test
    public void simpleTest() {
        ContactsPage contactsPage = new ContactsPage(driver);

        String expectedContact = "Chris Heavener";
        String expectedNumber = "+1(959)-1775994";

        String actualContact = contactsPage
                .searchContact(expectedContact)
                .getContact(0);

        Assert.assertEquals(actualContact, expectedContact);

        ContactDetailsPage contactDetailsPage = contactsPage.openResult(0);

        String fullname = contactDetailsPage.getFullname();
        Assert.assertEquals(fullname, expectedContact);

        String phoneNumber = contactDetailsPage.getPhoneNumber();
        System.out.println("Phone number is: " + phoneNumber);
        Assert.assertEquals(phoneNumber, expectedNumber);
    }
}
